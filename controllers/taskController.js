//controllers contain the function and business logic of our express.js application
//meaning all the operation it can dp will be placed in this file.

const Task = require('../models/task');

//controller function for getting all the tasks

module.exports.getAllTasks = () => {

	/*the return statement returns tha result of the mongoose method .find back to the "task.Route.js" which invokes this function
	when the /tasks route is accessed\
	.then method is used to wait for the mongoose .find method to finish before sending the result back to the route and
	eventually to the client	*/
	return Task.find({}).then(result =>{
		/* the "return" here, results of the mongoDB query to the "result" parameter defined in the "then" method */
		return result;

	})
}
// the request body coming from the client was pased from the "taskRoute.js" file via req.body as an argument and is renamed
//'requestBody' in parameter in the controller file.
module.exports.createTask = (requestBody) => {
	//create a task object based on the mongoose model task
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}

// delete task
//business logic
/*
1. look for the task with the corresponding ID
2. delete the task using the mongoose method "findByIdAndRemove" with the same
id provided in the route.
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}

// to update a task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				return updatedTask;
			}
		})
	})
}

//Activity

module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then((Task,err) =>{
		if(err){
			console.log(err);
			return false;
		} else{
			return Task;
		}
	})
}

//Activity Update status
module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		result.status = newStatus.status;
		return result.save().then((updatedStatus, saveErr) =>{
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				return updatedStatus;
			}
		})
	})
}
