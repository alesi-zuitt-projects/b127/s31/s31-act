// setup the dependencies
const express = require('express');
//creates router instance function as a middleware and routing system
/*allows access to http method middlewares that makes it easier to create routes
 for our application*/

const router = express.Router();

const taskController = require ('../controllers/taskController');

/*routes are responsible for defining the URIs that our client accesses and
the corresponding controller function that will be used when a route is
accessed */

// http:localhost:3001/getAll
router.get('/getAll', (req,res) => {
/*invokes the "getAllTasks" function from the "taskController.js" file and sends the results back to tha client */
	/* result controller is only used here to make the code to easier understand but its common practice
	to use the shorthand parameter name for a result using the parameter "result" or "res" */
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})



//create new task

router.post('/', (req,res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

//delete task
// /:id para unique specific na task
// ex : localhost:3001/tasks/123123123
router.delete('/:id',(req,res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})


//update task

router.put('/:id', (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})


//Activity

router.get('/:id', (req,res) =>{
	taskController.getOneTask(req.params.id).then(result => res.send(result));
})
module.exports = router;

//activity status
router.put('/:id/complete', (req,res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})

