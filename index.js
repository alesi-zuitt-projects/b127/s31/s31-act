// setup the dependencies
const express = require('express');
const mongoose = require('mongoose');
//this allows us to use all the routes
const taskRoute = require('./routes/taskRoute')

//server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

//database connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.nyots.mongodb.net/batch127_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true

		}
);

//connection error handling
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("we're connected to the cloud db"));

//add the task route
// http:localhost:3001/task
app.use('/tasks', taskRoute)

//Activity

app.listen(port, () => console.log(`now listening to port ${port}`))


// model -> controllers -> routes -> index.js